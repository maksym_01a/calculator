package sample;

public class Smoot
{
    private double feet;

    public Smoot(double f)
    {
        feet = f;
    }

    public double toSmoots()
    {
        return feet * 12.0 / 67.0;
    }
}
