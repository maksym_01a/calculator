package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

public class Controller
{
    @FXML
    TextField inputField;

    @FXML
    TextField resultField;

    public void handleSmootButton(ActionEvent ae)
    {
        double feet = Double.parseDouble(inputField.getText());
        Smoot s = new Smoot(feet);

        double smoots = s.toSmoots();
        resultField.setText("" + smoots);
    }

    public void handleClearButton(ActionEvent ae)
    {
        inputField.setText("");
        resultField.setText("");
    }
}
